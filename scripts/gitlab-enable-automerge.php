<?php

use Gitlab\Client;

require_once __DIR__ . "/../vendor/autoload.php";

$token = getenv('GITLAB_TOKEN');
$merge_request_id = getenv('CI_MERGE_REQUEST_IID');
$client = new Client();
$client->authenticate($token, Client::AUTH_OAUTH_TOKEN);
$project_id = getenv('CI_PROJECT_ID');

$mr_data = $client->mergeRequests()->show($project_id, $merge_request_id);
// If the title contains the word security with big letters, it's probably worth
// looking in to.
if (mb_strpos($mr_data["title"], '[SECURITY]') !== 0) {
  echo "This MR does not seem to be security related. Exiting";
  exit(0);
}

$data = [
  'merge_when_pipeline_succeeds' => TRUE,
];

$merge_enabled = FALSE;
$retries = 0;
while (!$merge_enabled) {
  try {
    echo "Trying to enable automerge. Retry number $retries\n";
    $client->mergeRequests()->merge($project_id, $merge_request_id, $data);
    // We suceeded!
    exit(0);
  }
  catch (\Throwable $e) {
    // Not sure what the error might be, but let's at least log it.
    echo sprintf("There was an error doing the automerge enable. Exception was %s and stack trace was %s \n", $e->getMessage(), $e->getTraceAsString());
  }
  $retries++;
  sleep(5);
  if ($retries >= 10) {
    throw new \Exception("Not able to enable automerge in $retries retries");
  }
}
